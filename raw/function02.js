$(document).ready(function() {
  // project tiles
    var $allTiles = $(".js-tile");
    var $Tiles = $(".Tiles");

    $(".Tiles > .Tile").each(function(i, el) {  
      
      var $tile = $(el);
      var target = $tile.children(".Tile-flyout");
      
      // get each items flyout height with extra buffer
      var thisHeight = target.children(".tile-content").outerHeight()+10;
      
      // Create ne timeline in paused state
      var tl = new TimelineMax({
        paused: true,
        reversed:true//,
        //forward: true // not a valid GSAP property 
      });

      TweenLite.set(target, {
        height:0,
        autoAlpha: 0,
        display: "none"//,
      });    
      // animate stuff
      tl.to(target, 1, {
        delay: 0.6,
        height: thisHeight,
        autoAlpha: 1,
        display: "block",
        ease: Cubic.easeInOut,  
        overwrite: "none"
      });
      // store timeline in DOM node
      el.animation = tl;
      
      // create element event handler
      $(el).on("click", function(event) {
        
        event.preventDefault();
        
        var that = this;
        var currentTile = $(this);
        
        $(".Tiles > .Tile.is-expanded").not(this).each(function(i, element) {
            console.log('reverse?');
            element.animation.reverse();
            currentTile.removeClass("not-expanded");
          $(window).scrollTop(1000);        
        }); 
        
        $allTiles.not(currentTile).removeClass("is-expanded");
        $allTiles.not(currentTile).removeClass("not-expanded");
        
        currentTile.toggleClass("is-expanded");

        if (this.animation.reversed()) {
              console.log('1');
              $allTiles.not(currentTile).addClass("not-expanded");
          
              this.animation.play();
              target.removeClass("reversing");
            $(window).scrollTop(1000);          
        } else {
              console.log('2');
              this.animation.reverse();
              target.addClass("reversing");
            $(window).scrollTop(1000);          
        }
      });  
    });
});
