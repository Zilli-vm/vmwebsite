$(document).ready(function() {
   
   /*vars */
   var navigationMenu =  $(".navigationMenu");
   var trigger =  $(navigationMenu).find(".trigger");
   var li = $(navigationMenu).find("menu li");
   var revealer =  $(navigationMenu).find(".revealer");   
   var menuWrapper =  $(navigationMenu).find(".menu-wrapper");
   
   /*Timeline*/
   var tlIn = new TimelineMax({ paused:true, onComplete: onComplete });
   var tlOut = new TimelineMax({ paused:true, onComplete: onComplete }); 
   var easeInOut =  Circ.easeInOut; 
   var easeIn = Circ.easeIn;
   var easeOut = Circ.easeOut;
   var speed = 0.5;
   var status = "close";
   var canAnimate = true;

   /*hide show nav*/
   var didScroll;
   var lastScrollTop = 0;
   var delta = 5;
   var navbarHeight = $('header').outerHeight();


$(window).scroll(function(event){
    didScroll = true;
});

setInterval(function() {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();
    
    // Make sure they scroll more than delta
    if(Math.abs(lastScrollTop - st) <= delta)
        return;
    
    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > navbarHeight){
        // Scroll Down
        $('header').removeClass('nav-down').addClass('nav-up');
    } else {
        // Scroll Up
        if(st + $(window).height() < $(document).height()) {
            $('header').removeClass('nav-up').addClass('nav-down');
        }
    }
    
    lastScrollTop = st;
}

   
   /*setMenuHeight*/
   function setMenuHeight() {
     $(menuWrapper).slimScroll({
       height: document.documentElement.clientHeight,
       color: "transparent"
     });
    }
     setMenuHeight();
     $(window).resize(function() {
       setMenuHeight();
     });

   function onComplete() {
     canAnimate = true;
     $(navigationMenu).toggleClass("active");
  }
  tlIn
    .fromTo(
      revealer, 
      speed, 
      {
        x: "0%",
        width: "0%",
        ease:easeIn
      }, 
      {
        width: "100%", 
        ease: easeInOut
      }
    )
    .to(revealer,speed, { x: "100%" , ease:easeOut})
    .set(revealer, {x: "O%", width:"0%" })
    .fromTo(
     menuWrapper,
     speed,
     {
       x:"-100%", 
      ease:easeIn  
     },
     {
      x:"0%", 
      ease:easeOut 
     },
      "-=0.5"
   )
   .staggerFromTo(li, speed, { x: "-120%" }, { x: "0%" }, 0.05,"-=0.5");

   tlOut
   .fromTo(revealer, speed, {
      x: "0%",
      width: "0%",
      ease:easeIn
    }, 
    {
      width: "100%", 
      ease: easeInOut
    }
  )
  .to(revealer,speed, { x: "100%" , ease:easeOut })
  .set(revealer, {x: "O%", width:"0%" })
  .to(menuWrapper,speed,{ x:"100%", ease:easeOut },"-=0.5")
  .set(menuWrapper,{ x: "-100%}"});

  trigger.on("click", toggleMenu);
    
  function toggleMenu() {
     if (canAnimate == true ) {
      canAnimate == false; 
      if(status == "close") {
        tlIn.play(0);
        status = "open";
      }
      else {
        tlOut.play(0);
        status = "close";
      }
     } else return;   
  }  
});

gsap.registerPlugin(ScrollTrigger);

gsap.from(".logo-v", { 
  rotation: 15, 
  y: 15, 
  duration: 0.5 
})

gsap.fromTo(".logo-v", 1, {
   opacity: 0, 
   y: 0
  }, 
  {
    opacity: 1 
  })

gsap.from("#small-id", { 
  rotation: 15, 
  y: 0, 
  duration: 0.5 
})

gsap.fromTo("#small-id", 1, {
   opacity: 0, 
   y: 0
  }, 
  {
    opacity: 1 
  })

gsap.from("#section-1", 1, {
   scrollTrigger: {
      trigger: "#section-1 .col-4",
      toggleActions: "play none none none",      
      start: "5% 75%",
      markers: false,
   },
   opacity: 0, 
   y: 50,
   duration: 0.2
  }, {
    opacity: 1 
  })

gsap.from("#section-2 .col-12", 1, {
   scrollTrigger: {
      trigger: "#section-2 .container",
      markers: false,
      toggleActions: "play none none reverse",      
      start: "90% 70%",
   },
   opacity: 0, 
   y: 50,
   duration: 0.2
  }, 
  {
   opacity: 1 
  })

gsap.from("#section-2-intro .col-12", 1, {
   opacity: 0, 
   y: 50,
   duration: 0.2
  }, 
  {
   opacity: 1 
  })

gsap.from("#section-2-am .col-12", 1, {
   scrollTrigger: {
      trigger: "#section-2-am .col-12",
      markers: false,
      toggleActions: "play none none none",      
      start: "5% 80%",
   },
   opacity: 0, 
   y: 50,
   duration: 0.2
  }, 
  {
   opacity: 1 
  })

gsap.from("#section-2-am .col-6", 1, {
   scrollTrigger: {
      trigger: "#section-2-am .col-6",
      markers: false,
      toggleActions: "play none none none",      
      start: "5% 80%",
   },
   opacity: 0, 
   y: 50,
   duration: 0.2
  }, 
  {
   opacity: 1 
  })

gsap.from("#section-3 .col-3", 1, {
   scrollTrigger: {
      trigger: "#section-3",
      markers: false,
      toggleActions: "play none none reverse",      
      start: "10% 90%",
   },
   opacity: 0, 
   y: 50,
   duration: 0.2
  }, 
  {
   opacity: 1 
  })

gsap.from("#section-3 .col-12", 1, {
   scrollTrigger: {
      trigger: "#section-3",
      markers: false,
      toggleActions: "play none none reverse",      
      start: "10% 90%",
   },
   opacity: 0, 
   y: 50,
   duration: 0.2
  }, 
  {
   opacity: 1 
  })

gsap.fromTo("#introquote", 1, {
   opacity: 0, 
   y: 0 
  }, 
  { 
    opacity: 1     
  })

gsap.from("#project-1a .project-intro", 1, {
   opacity: 0, 
   x: -1000, 
  }, 
  { 
    x: 0,
    opacity: 1,
    duration: 0.2,  
    ease: "power1.out",        
  });

gsap.from("#project-1b .project-intro", 1, {
   opacity: 0, 
   x: -1000, 
  }, 
  { 
    x: 0,
    opacity: 1,
    duration: 0.2,  
    ease: "power1.out",        
  });

var left = new TimelineMax ();



